<?php
define("P1_ROOT",dirname(__DIR__) . '/');
if(file_exists(P1_ROOT . 'install/temp.txt')){
	$filename = P1_ROOT . 'install/temp.txt';
	if(filesize($filename) > 0){
		$file = fopen($filename, "r");
		$tempFile = fread($file, filesize($filename));
		fclose($file);
		$contents = json_decode($tempFile, true);
	}
}

if(isset($_POST['l1'])){
	$url = 'http://eternitech.tk/mothership/licence';
	$myvars = 'key=' . $_POST['l1'] . '-' . $_POST['l2'] . '-' . $_POST['l3'] . '-' . $_POST['l4'];
	$key = $_POST['l1'] . '-' . $_POST['l2'] . '-' . $_POST['l3'] . '-' . $_POST['l4'];
	
	$ch = curl_init( $url );
	curl_setopt( $ch, CURLOPT_POST, 1);
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt( $ch, CURLOPT_HEADER, 0);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
	
	$response = curl_exec( $ch );
	if($response == 1){
	    $stage = 2;
		//CREATE FILES CONTENT
		$array = array('licence_key' => $key);
		$txt = json_encode($array);
		//WRITE LICENCE KEY TO FILE
		$file = fopen(P1_ROOT . "install/temp.txt", "w") or die("Unable to create temp file, please create a file 'temp.txt' in the install directory, chmod it to '666' and refresh this page.");
		fwrite($file, $txt);
		fclose($file);
	}else{
		$stage = 1;
		$error = 'I had an issue registering with that licence key, could you check it for me?';
	}
}elseif(isset($_POST['dbCon'])){
	try{
		$db = new \PDO("mysql:host=" . $_POST['dbserver'] . ";dbname=" . $_POST['dbname'], $_POST['dbuser'], $_POST['dbpass']);
	}catch(Exception $e){
		$stage = 2;
		$error = 'There was an error connecting to the database:<br/>' . $e;
	}
	if(!isset($error)){
		//CREATE FILES CONTENT
		$contents['dbserver'] = $_POST['dbserver'];
		$contents['dbname'] = $_POST['dbname'];
		$contents['dbuser'] = $_POST['dbuser'];
		$contents['dbpass'] = $_POST['dbpass'];
		$txt = json_encode($contents);
		//WRITE DB TO FILE
		$file = fopen(P1_ROOT . "install/temp.txt", "w") or die("Unable to access temp file, please check that it's chmodded to '666', then refresh this page.");
		fwrite($file, $txt);
		fclose($file);
		
		//CREATE DATABASE
		include('database.php');
		
		//NEXT STAGE
		$stage = 3;
	}
}elseif(isset($_POST['suCon'])){
	if(isset($contents['suEmail'])){
		$stage = 4;
		$error = "Error: Super-user account already exists";
	}
	
	try{
		$db = new \PDO("mysql:host=" . $contents['dbserver'] . ";dbname=" . $contents['dbname'], $contents['dbuser'], $contents['dbpass']);
	}catch(Exception $e){
		$stage = 2;
		$error = 'There was an error connecting to the database:<br/>' . $e;
	}
	
	//VALIDATE INFORMATION
	if($_POST['suEmail'] == "" || $_POST['suPassword'] == "" || $_POST['suName'] == "" || strlen($_POST['suPassword']) < 8 || !preg_match("#[0-9]+#", $_POST['suPassword'])){
		//COMPLETE FORM ERROR MESSAGE
		$error = "Error: I think the form wasn't filled out correctly? Could you check that your password is over 8 chars and contains numbers? Thanks :)";
		$stage = 3;
	}
	
	//ADD USER TO DATABASE
	if(!isset($error)){
		$userid = uniqid();
		$pass = password_hash($_POST['suPass'], PASSWORD_DEFAULT);
		try{
			//INSERT MAIN USER ROW
			$userInsert = $db->prepare("INSERT INTO `users` (`id`,`email`,`pass`,`name`,`permissions`) VALUES (?,?,?,?,?)");
			$userInsert->bindValue(1,$userid);
			$userInsert->bindValue(2,$_POST['suEmail']);
			$userInsert->bindValue(3,$pass);
			$userInsert->bindValue(4,$_POST['suName']);
			$userInsert->bindValue(5,4);
			$userInsert->execute();
			
			//INSERT USER META ROW
			$metaInsert = $db->prepare("INSERT INTO `users_meta` (`id`) VALUES (?)");
			$metaInsert->bindValue(1,$userid);
			$metaInsert->execute();
		}catch(Exception $e){
			$error = "Error: I couldn't create your super user account: " . $e;
			$stage = 3;
		}
	}
	
	if(!isset($error)){
		//WRITE SU TO FILE
		$contents['suEmail'] = $_POST['suEmail'];
		$contents['suName'] = $_POST['suName'];
		$txt = json_encode($contents);
		$file = fopen(P1_ROOT . "install/temp.txt", "w") or die("Unable to access temp file, please check that it's chmodded to '666' and then refresh this page.");
		fwrite($file, $txt);
		fclose($file);
		
		//NEXT STAGE
		$stage = 4;
	}
}elseif(isset($_POST['siteEnv'])){
		//WRITE DB TO FILE
		$contents['siteName'] = $_POST['siteName'];
		$contents['siteUrl'] = $_POST['siteUrl'];
		$txt = json_encode($contents);
		$file = fopen(P1_ROOT . "install/temp.txt", "w") or die("Unable to access temp file, please check that it's chmodded to '666' and then refresh this page.");
		fwrite($file, $txt);
		fclose($file);
		
		//NEXT STAGE
		$stage = 5;
}else{
	$stage = 1;
}
?>
<!DOCTYPE>
<head>
	<title>Install Weld</title>
	<link href="/install/assets/main.css" rel="stylesheet" type="text/css" />
	<script href="/install/assets/main.js" type="text/javascript"></script>
	<link rel="icon" href="/install/assets/icon.ico" type="image/icon"/>
</head>
<body>
	<div class="main">
		<img src="/install/assets/logo.svg" alt="weld logo" class="login-logo" /><br/><hr class="line-sep" />
		<h1 style="margin:0;">Let's get set up!</h1><hr class="line-sep" /><br/>
		Step <?= $stage; ?>/5<br/><br/>
		<div class="progress" style="background-color:<?php if($stage >= '1'){echo'#06c200';}else{echo'#828282';} ?>;"></div><div class="progress" style="background-color:<?php if($stage >= '2'){echo'#06c200';}else{echo'#828282';} ?>;"></div><div class="progress" style="background-color:<?php if($stage >= '3'){echo'#06c200';}else{echo'#828282';} ?>;"></div><div class="progress" style="background-color:<?php if($stage >= '4'){echo'#06c200';}else{echo'#828282';} ?>;"></div><div class="progress" style="background-color:<?php if($stage >= '5'){echo'#06c200';}else{echo'#828282';} ?>;"></div>
		<?php if(isset($error)){ echo '<div class="error">' . $error . '</div>';} ?>
		<br/><br/><div class="card">
		<?php
		if($stage == 1){
			?><!-- LICENCE KEY -->
				<div class="main" id="1">
					<h3>Please input your licence key:</h3>
					<form action="" method="POST">
						<input type="text" name="l1" style="width:10%;" maxlength="5" value="<?= $_POST['l1']; ?>" /> - <input type="text" name="l2" style="width:10%;" maxlength="5" value="<?= $_POST['l2']; ?>" /> - <input type="text" name="l3" style="width:10%;" maxlength="5" value="<?= $_POST['l3']; ?>" /> - <input type="text" name="l4" style="width:10%;" maxlength="5" value="<?= $_POST['l4']; ?>" />
						<br/><br/><input type="submit" class="button" value="Register Weld" /><br/><br/><br/>
						Don't have a licence key? Don't worry! <a href="http://weld.tk/licence">They're free</a> :)
					</form>
				</div>
			<?php
		}elseif($stage == 2){
			?>
			<!-- DATABASE CREDENTIALS -->
			<div class="main" id="2">
				<h3 style="padding:0;margin:0;">Please set up my database</h3>
				(I need an empty database to setup)<br/><br/>
				<form action="" method="POST">
					<label for="server">Server IP/Hostname (with ports)</label><br/>
					<input type="text" name="dbserver" style="width:20%;" value="<?= $_POST['dbserver']; ?>" /><br/><br/>
					<label for="name">Database name</label><br/>
					<input type="text" name="dbname" style="width:20%;" value="<?= $_POST['dbname']; ?>" /><br/><br/>
					<label for="user">Database Username</label><br/>
					<input type="text" name="dbuser" style="width:20%;" value="<?= $_POST['dbuser']; ?>" /><br/><br/>
					<label for="pass">Password</label><br/>
					<input type="password" name="dbpass" style="width:20%;" /><br/><br/>
					<input type="submit" class="button" value="Check connection" name="dbCon" /><br/><br/><br/>
				</form>
			</div>
			<?php
		}elseif($stage == 3){
			?>
			<!-- SUPER USER DETAILS -->
			<br/><div class="pass">Database created successfully</div>
			<div class="main" id="3">
				<h3 style="padding:0;margin:0;">Hi, I'm Weld, and you are..?</h3>
				Let's get you set up<br/><br/>
				<form action="" method="POST">
					<label for="name">What's your name? (superuser account)</label><br/>
					<input type="text" name="suName" style="width:20%;" value="<?= $_POST['suName']; ?>" /><br/><br/>
					<div style="display:inline-block;">
						<label for="name">super-user email</label><br/>
						<input type="email" name="suEmail" style="width:80%;" value="<?= $_POST['suEmail']; ?>" />
					</div><div style="display:inline-block;">
						<label for="user">super-user password</label><br/>
					<input type="password" name="suPassword" style="width:80%;" value="<?= $_POST['suPassword']; ?>" />
					</div><br/><br/>
					<input type="submit" class="button" value="setup my account" name="suCon" /><br/><br/><br/>
				</form>
			</div>
			<?php
		}elseif($stage == 4){
			?>
			<!-- WELD ENV DETAILS -->
			<div class="main" id="3">
				<h3 style="padding:0;margin:0;">Last question, where am I?</h3>
				Let's get you set up<br/><br/>
				<form action="" method="POST">
					<label for="siteName">Website Name</label><br/>
					<input type="text" name="siteName" style="width:20%;" value="<?= $_POST['siteName']; ?>" /><br/><br/>
					<label for="siteUrl">Website Url</label><br/>
					<input type="text" name="siteUrl" style="width:20%;" value="<?= $_POST['siteUrl']; ?>" /><br/><br/>
					<input type="submit" class="button" value="Contact mothership and register" name="siteEnv" /><br/><br/><br/>
				</form>
			</div>
			<?php
		}elseif($stage == 5){
			//CONTACT MOTHERSHIP
			$url = 'http://eternitech.tk/mothership/licence';
			$myvars = 'key=' . $contents['licence_key'] . '&url=' . $contents['siteUrl'] . '&name=' . $contents['siteName'];
			
			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_POST, 1);
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt( $ch, CURLOPT_HEADER, 0);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
			
			$response = curl_exec( $ch );
			if($response == 1){
				//FORMAT INI FILE FROM TEMPLATE
				$TPL = fopen('ini.tpl', "r") or die('I can\'t find the ini template? Could you check your download? Thanks!');
				$TPL = fread($TPL,filesize('ini.tpl'));
				preg_match_all('/{{(.*)}}/',$TPL, $tplVars);
				foreach ($tplVars[1] as $value) {
					$thing = "{{" . $value . "}}";
					$TPL = str_replace($thing, $contents[$value], $TPL);
				}
				//CREATE P1 APPLICATION.INI
				$file = fopen("../application.ini", "w") or die("Unable to create ini file, please create application.ini in the weld root folder, chmod it to '666' and refresh this page.");
				fwrite($file, $TPL);
				fclose($file);

				//DELETE temp.txt FILE
				unlink(P1_ROOT . "install/temp.txt") or die('Failed to delete temp file. Please delete manually.');
				
				echo "<h1>We're all done!</h1>Thanks for using weld! I'm all set up no so we can <a href=\"/weld/\">begin!</a>";
			}else{
				$stage = 1;
				$error = 'I had an issue registering with that licence key, could you check it for me?';
			}
		} ?>
		</div><br/><br/>
	</div>
</body>