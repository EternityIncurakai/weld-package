;THIS IS THE CONFIG FILE FOR THE P1 FRAMEWORK
;THE P1 FRAMEWORK IS PART OF THE WELD PACKAGE FROM ETERNITECH
;THANK YOU FOR USING P1
[application]
name = "{{siteName}}"
url = "{{siteUrl}}"

[database]
dbcon = "{{dbserver}}"
dbname = "{{dbname}}"
dbpass = "{{dbpass}}"
dbuser = "{{dbuser}}"

[owner]
name = "{{suName}}"
email = "{{suEmail}}"

[misc]
installed = "true"
timezone = ""
licence_key = "{{licence_key}}"
googAna = ""

;Environment can be 'development' or 'production'
[environment]
type = "production"

;active can = 'true', ''
;types can = 'light','heavy','scheduled'
[maintenance]
active = ""
type = ""
end = ""
reason = ""