<?php
//P1 DATABASE SETUP PROCESS
//LAST UPDATED 13/06/2015
//VERSION 0.2

//CREATE USERS TABLE
$db->query("CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(250) NOT NULL COMMENT 'unique userId',
  `email` varchar(250) NOT NULL COMMENT 'user''s e-mail address',
  `pass` varchar(250) NOT NULL COMMENT 'user''s password',
  `permissions` int(250) NOT NULL DEFAULT '0' COMMENT 'user''s permissions',
  `name` varchar(250) NOT NULL COMMENT 'display name',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

//CREATE USERS_FOLLOW TABLE
$db->query("CREATE TABLE IF NOT EXISTS `users_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_follower` varchar(250) NOT NULL,
  `id_followed` varchar(250) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

//CREATE USERS_META TABLE
$db->query("CREATE TABLE IF NOT EXISTS `users_meta` (
  `id` varchar(250) NOT NULL COMMENT 'user id from users table',
  `DOB` date NOT NULL COMMENT 'users date of birth (YYYY-MM-DD)',
  `description` varchar(250) NOT NULL DEFAULT 'Placeholder description.' COMMENT 'User''s description on profile.',
  `website` varchar(250) NOT NULL DEFAULT 'http://example.com' COMMENT 'User''s website on profile page',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

//CREATE WELDANALYTICS TABLE
$db->query("CREATE TABLE IF NOT EXISTS `weldanalytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(2) NOT NULL,
  `day` int(2) NOT NULL,
  `value` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;");

//CREATE WELDOPTIONS TABLE
$db->query("CREATE TABLE IF NOT EXISTS `weldoptions` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `optionName` varchar(50) NOT NULL,
  `optionValue` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;");
?>
