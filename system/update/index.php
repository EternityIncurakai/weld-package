<?php
/*
 * ===========================
 * ===== P1/Weld Updater =====
 * Created by: Peter Richards
 * Created on: 25/5/15
 * ===========================
 */
 
 /*
 *
 * This updater is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */
 
 if($_GET['update'] != "1"){
 	die();
 }
 
 //DEFINE STARTUP CLASS
 include('../../system/controllers/startup.php');
 $start = new Startup;
 
 //CONTACT MOTHERSHIP FOR MORE INFORMATION
 $myvars = 'key=' . $start->iniGet('misc','licence_key');
 
 $ch = curl_init('http://eternitech.tk/mothership/update?info=1');
 curl_setopt( $ch, CURLOPT_POST, 1);
 curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
 curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
 curl_setopt( $ch, CURLOPT_HEADER, 0);
 curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
 
 $response = curl_exec( $ch );
 $response = json_decode($response, true);
 
 //DEFINE STARTUP CONSTANTS AGAIN
 $start->defineConst();
 
 //CHECK IF UPDATE IS LEGITIMATE
 if(!($response['number'] > P1_VERSION)){
 	die;
 }
 
 //DOWNLOAD .ZIP FILE
 file_put_contents("update.zip", fopen("http://eternitech.tk/application/mothership/assets/versions/" . $response['number'] . ".zip", 'r'));
 
 //UNZIP FILE
 $zip = new ZipArchive;
 $res = $zip->open(P1_ROOT . 'system/update/update.zip');
 if($res == TRUE){
	$zip->extractTo(P1_ROOT);
	$zip->close();
	unlink(P1_ROOT . 'system/update/update.zip');
 	echo 1;
 }else{
 	echo 0;
 }
 
 ?>