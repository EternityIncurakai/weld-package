<?php
/*
 *
 * This themes class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class theme extends bootstrap {
	
	public function construct($url) {
		//DEFINE INI FILE
		$bs = new bootstrap;
		$inifile = parse_ini_file($bs->getAppDir($url) . 'application.ini',true);
		
		//DEFINE CONSTANT
		//define("THEME_NAME",$inifile['application']['theme']);
		$GLOBALS["theme_name"] = $inifile['application']['theme'];
		
		if($inifile['application']['theme'] == NULL){
			throw new Exception('Please install the P1 framework before continuing.');
		}
		
		//DEFINE THEME_DIR
		$GLOBALS["THEME_DIR"] = P1_ROOT . "customisation/themes/" . $GLOBALS["theme_name"] . "/";
		//if(!defined('THEME_DIR')){define("THEME_DIR",P1_ROOT . "customisation/themes/" . THEME_NAME . "/");}
	}
	
	public function setConstants() {
		$manifestFile = $GLOBALS["THEME_DIR"] . "manifest.json";
		if(file_exists($manifestFile)){
			//GRAB MANIFEST
			$manifestActual = fopen($manifestFile, "r");
			$manifest = json_decode(fread($manifestActual,filesize($manifestFile)),true);
			
			//DEFINE PATHS
			$assetsPath = $GLOBALS["THEME_DIR"] . $manifest['assetDir'];
			$cssPath = $GLOBALS["THEME_DIR"] . "/css/main.css";
			$faviconPath = $GLOBALS["THEME_DIR"] . $manifest['assetDir'] . "icon.ico";
			
			//DEFINE CONSTANTS FOR THEME
			$GLOBALS["THEME_ASSETS"] = $assetsPath;
			$GLOBALS["THEME_CSS"] = $cssPath;
			$GLOBALS["THEME_FAVICON"] = $faviconPath;
		}else{
			throw new Exception('Please install the P1 framework before continuing.');
		}
	}
	
	public function verify() {
		//GRAB MANIFEST
		$manifestFile = $GLOBALS["THEME_DIR"] . "manifest.json";
		$manifestActual = fopen($manifestFile, "r");
		$manifest = json_decode(fread($manifestActual,filesize($manifestFile)),true);
		
		//VERIFY MANIFEST
		if($GLOBALS["theme_name"] != $manifest['name'] || P1_VERSION < $manifest['requiredVersion']){
			 throw new Exception('Your theme manifest has been corrupted, please re-install the theme.');
		}
	}
	
	public function getLink($link) {
		if($link == 'CSSLink'){
			return P1_URL . "/customisation/themes/" . $GLOBALS["theme_name"] . "/css/main.css";
		}elseif($link == 'faviconLink'){
			return P1_URL . "/customisation/themes/" . $GLOBALS["theme_name"] . "/assets/favicon.ico";
		}
	}
	
}