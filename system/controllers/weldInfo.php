<?php
/*
 *
 * The weldInfo class file is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

 class info {
     
	 private $db;
	 
     function __construct() {
		//SETUP DATABASE
		$this->db = $GLOBALS['db'];
     }
	 
	 function getAppNo(){
	 	$dirno = scandir(P1_ROOT . 'application');
		$dirno = count($dirno);
		return $dirno - 2;
	 }
	 
	 function getAccNo(){
	 	$result = $this->db->prepare("SELECT * FROM `users`");
		$result->execute();
		return $result->rowCount();
	 }
	 
	 function getThemeNo(){
	 	$dirno = scandir(P1_ROOT . 'customisation/themes');
		$dirno = count($dirno);
		return $dirno - 2;
	 }
	 
	 function upToDate(){
	 	 //CONTACT MOTHERSHIP FOR VERSION NUMBER
	 	 $info = file_get_contents('http://eternitech.tk/mothership/update?info=1');
		 $info = json_decode($info, true);
		 //COMPARE AGAINST INSTALLED VERSION
		 if(!($info['number'] > P1_VERSION)){
		 	return 1;
		 }else{
		 	return 0;
		 }
	 }
	 
	 function googAnalytics($arg){
	 	//CHECK INI FILE
	 	require_once(P1_ROOT . 'system/controllers/startup.php');
		$start = new startup;
		
		//LOGIC
	 	if($arg = "on"){
	 		if($start->iniGet('misc','googAna') == ""){
	 			return 0;
	 		}else{
	 			return 1;
	 		}
	 	}elseif($arg = "code"){
	 		return $start->iniGet('misc','googAna');
	 	}
	 }
	 
 }
 