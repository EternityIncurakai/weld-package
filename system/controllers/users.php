<?php
/*
 *
 * This users class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class users {
	
	private $db;
	private $ranks = array(
		1 => 'Member',
		2 => 'Moderator',
		3 => 'Admin',
		4 => 'Super-user'
	);
	
	//START SESSION ENVIRONMENT AND GIVE ACCESS TO DATABASE
	function __construct() {
		//START SESSION ON CONSTRUCT
		session_start();
		
		//SETUP DATABASE
		$this->db = $GLOBALS['db'];
	}
	
	
	//PASSWORD ENCRYPTION FUNCTION
	public function encrypt($string){
		return password_hash($string, PASSWORD_DEFAULT);
	}
	
	
	//CHECK A USERS PASSWORD AND LOG THEM IN
	public function authUser($email,$pass){
		//GRAB USER FROM DATABASE
		$loginResult = $this->db->prepare("SELECT * FROM `users` WHERE `email`=?");
		$loginResult->bindValue(1,$email);
		$loginResult->execute();
		$row = $loginResult->fetch();
		
		//VERIFY PASSWORD OR RETURN ERROR
		if(password_verify($pass, $row['pass'])){
			$this->setSession($row['id']);
			return true;
		}else{
			return 'Error: Email or Password is incorrect';
		}
	}
	
	
	//ADD USER TO DATABASE FUNCTION
	public function createUser($email, $pass, $name, $dob){
		//VALIDATE INFORMATION
		if($email == "" || $pass == "" || $name == "" || $dob == "" || strlen($pass) < 8 || !preg_match("#[0-9]+#", $pass)){
			//COMPLETE FORM ERROR MESSAGE
			return '2';
		}
		
		//CHECK IF EMAIL ALREADY EXISTS
		$checkResult = $this->db->prepare('SELECT * FROM `users` WHERE `email`=?');
		$checkResult->bindValue(1,$email);
		$checkResult->execute();
		$row = $checkResult->fetch();
		
		//IF USER DOESN'T EXIST, INSERT USER ROW ELSE ERROR
		if($row['id'] == NULL){
			//CREATE VARS
			$userid = uniqid();
			$pass = $this->encrypt($pass);
			
			//INSERT MAIN USER ROW
			$userInsert = $this->db->prepare("INSERT INTO `users` (`id`,`email`,`pass`,`name`) VALUES (?,?,?,?)");
			$userInsert->bindValue(1,$userid);
			$userInsert->bindValue(2,$email);
			$userInsert->bindValue(3,$pass);
			$userInsert->bindValue(4,$name);
			$userInsert->execute();
			
			//INSERT USER META ROW
			$metaInsert = $this->db->prepare("INSERT INTO `users_meta` (`id`,`dob`) VALUES (?,?)");
			$metaInsert->bindValue(1,$userid);
			$metaInsert->bindValue(2,$dob);
			$metaInsert->execute();
			
			$this->setSession($userid);
			
			return TRUE;
		}else{
			//EMAIL ALREADY IN USE ERROR MESSAGE
			return '3';
		}
	}
	
	
	//GET USER ID FROM EMAIL
	public function getUserId($email){
		//QUERY DATABASE FOR USER ROW
		$getUser = $this->db->prepare("SELECT * FROM `users` WHERE `email`=?");
		$getUser->bindValue(1,$email);
		$getUser->execute();
		$row = $getUser->fetch();
		
		//SET ID VARIABLE AND RETURN OR RETURN FALSE IF ID NULL
		$id = $row['id'];
		if ($id != ""){return $id;}else{return FALSE;}
	}
	
	
	//DELETE AN ACCOUNT FROM THE DATABASE
	public function removeUser($id){
		//QUERY IF ROW EXISTS
		$getUser = $this->db->prepare("SELECT * FROM `users` WHERE `id`=?");
		$getUser->bindValue(1,$id);
		$getUser->execute();
		$row = $getUser->fetch();
		if($row !== NULL){
			$deleteUser = $this->db->prepare("DELETE FROM `users` WHERE `id`=?");
			$deleteUser->bindValue(1,$id);
			$deleteMeta = $this->db->prepare("DELETE FROM `users_meta` WHERE `id`=?");
			$deleteUser->execute();
			$this->logout();
		}else{
			return 0;
		}
	}
	
	
	//BEGIN SESSION
	public function setSession($id){
		$_SESSION['id'] = $id;
	}
	
	
	//END SESSION FUNCTION
	public function logout(){
       session_unset();
       session_destroy();
       session_write_close();
       return true;
	}
	
	
	//GET DB VARS FUNCTION
	public function __get($key) {
		//QUERY USERS TABLE
		$userGet = $this->db->prepare("SELECT * FROM `users` WHERE `id`=?");
		$userGet->bindValue(1,$_SESSION['id']);
		$userGet->execute();
		$row = $userGet->fetch();
		
		//QUERY META TABLE
		$metaGet = $this->db->prepare("SELECT * FROM `users_meta` WHERE `id`=?");
		$metaGet->bindValue(1,$_SESSION['id']);
		$metaGet->execute();
		$mrow = $metaGet->fetch();
		switch($key){
			case 'permissions' :
				return $row['permissions'];
			case 'rank' : 
				return $this->ranks[$row['permissions']];
			case 'name' :
				return $row['name'];
			case 'email' :
				return $row['email'];
			case 'enabled' :
				return $row['enabled'];
			case 'site' :
				return $mrow['website'];
			case 'description' :
				return $mrow['description'];
			case 'dob' :
				return $mrow['dob'];
			default :
				return;
		}
    }
	
	//SET DB VARS FUNCTION
	public function __set($key, $value) {
		//QUERY USERS TABLE
		$update = $this->db->prepare("UPDATE `users` SET `" . $key . "`=? WHERE `id`=?");
		$update->bindValue(1,$value);
		$update->bindValue(2,$_SESSION['id']);
		$update->execute();
		/*
		//QUERY META TABLE
		$update = $this->db->prepare("SELECT * FROM `users_meta` WHERE `id`=?");
		$update->bindValue(1,$_SESSION['id']);
		$update->execute();
		switch($key){
			case 'permissions' :
				return $row['permissions'];
			case 'name' :
				return $row['name'];
			case 'email' :
				return $row['email'];
			case 'site' :
				return $mrow['website'];
			case 'description' :
				return $mrow['description'];
			case 'dob' :
				return $mrow['dob'];
			default :
				return;
		}*/
    }
	
}
?>