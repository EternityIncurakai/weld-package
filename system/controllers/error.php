<?php
/*
 *
 * This error class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class err {

	public function genError() {
		if(file_exists(P1_ROOT . 'customisation/errors/general.php')){
			include(P1_ROOT . 'customisation/errors/general.php');
		}else{
			$this->fatal('Couldn\'t find general error file.', 'The following error can be fixed from the Weld control panel if available.');
		}
		die;
	}
	
	public function lightMaintenance() {
		if(file_exists(P1_ROOT . 'customisation/errors/maintenance_light.php')){
			include(P1_ROOT . 'customisation/errors/maintenance_light.php');
		}else{
			$this->fatal('Couldn\'t find light maintenance error file.', 'The following error can be fixed from the Weld control panel if available.');
		}
		die;
	}
	
	public function heavyMaintenance($end, $reason) {
		if(file_exists(P1_ROOT . 'customisation/errors/maintenance_heavy.php')){
			include(P1_ROOT . 'customisation/errors/maintenance_heavy.php');
		}else{
			$this->fatal('Couldn\'t find heavy maintenance error file.', 'The following error can be fixed from the Weld control panel if available.');
		}
		echo "<br/><br/>
			<h2 style=\"padding:0;margin:0;\">Reason</h2>" . $reason . "<br/><br/>
			<h2 style=\"padding:0;margin:0;\">Estimated end time</h2>" . $end . "
		</div>";
		die;
	}
	
	public function schMaintenance($end, $reason) {
		if(file_exists(P1_ROOT . 'customisation/errors/maintenance_scheduled.php')){
			include(P1_ROOT . 'customisation/errors/maintenance_scheduled.php');
		}else{
			$this->fatal('Couldn\'t find scheduled maintenance error file.', 'The following error can be fixed from the Weld control panel if available.');
		}
		echo "<br/><br/>
			<h2 style=\"padding:0;margin:0;\">Reason</h2>" . $reason . "<br/><br/>
			<h2 style=\"padding:0;margin:0;\">Estimated end time</h2>" . $end . "
		</div>";
		die;
	}

	public function pageBuild($type, $content) {
		if($type == "path"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Path too long, P1 will not handle a file of path: " . $content,"The following error would lead to a 404 page in production mode.");
			}else{
				$bs->buildPage('/404');
			}
		}
		
		if($type == "404"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Page doesn't exist, P1 cannot find file of path: " . $content,"The following error would lead to a 404 page in production mode.");
			}else{
				$bs->buildPage('404');
			}
		}
		
		if($type == "meta"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Meta file issue, please check the info file in: " . $content,"The following error would lead to a general error page in production mode.");
			}else{
				$this->genError();
			}
		}
		
		if($type == "invalidmeta"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Invalid Meta template, please check the template file in: " . $content,"The following error would lead to a general error page in production mode.");
			}else{
				$this->genError();
			}
		}
		
		if($type == "pagetpl"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Invalid page template, please check the template file in: " . $content,"The following error would lead to a general error page in production mode.");
			}else{
				$this->genError();
			}
		}
		
		if($type == "themeInfo"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Invalid theme json, please check the json file in: " . $content,"The following error would lead to a general error page in production mode.");
			}else{
				$this->genError();
			}
		}
		
		if($type == "themeAsset"){
			require_once P1_ROOT . 'system/controllers/bootstrap.php';
			$bs = new bootstrap;
			if(ENVIRONMENT == "development"){
				$this->fatal("Invalid theme asset in tpl: <i>\"" . $content,"The following error would be surpressed in production mode");
			}
		}
	}
	
	public function fatal($message,$notice) {
		if(isset($notice)){ echo "<br/><b>P1 - Fatal - Added notice:</b> " . $notice; }
		throw new Exception("<br/><b>P1 - Fatal error:</b> " . $message . "<br/>");
	}
}
