<?php
/*
 *
 * This startup class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class Startup {

	function __construct() {
		define("P1_ROOT",dirname(dirname(__DIR__)) . '/');
	}	
	
	public function checkini() {
		$inifile = parse_ini_file('application.ini',true);
		if($inifile['misc']['installed'] != "true"){
			throw new Exception('Please install the P1 framework before continuing.');
		}
	}
	
	public function iniChecks() {
		require_once('system/controllers/error.php');
		$err = new err;
		
		$inifile = parse_ini_file('application.ini',true);
		//CHECK WHAT MODE THE ENVIRONMENT IS IN
		if($inifile['environment']['type'] == NULL){
			throw new Exception('Cannot get environment type');
		}else{
			if($inifile['environment']['type'] != "development" && $inifile['environment']['type'] != "production"){throw new Exception('Cannot get environment type.');}
			define("ENVIRONMENT", $inifile['environment']['type']);
		}
		
		//CHECK IF THE DATABASE HAS BEEN CONNECTED
		if($inifile['database']['dbcon'] == NULL){
			if(ENVIRONMENT == "development"){throw new Exception('Can\'t get db credentials');}else{$err->genError();}
		}
		
		//CONNECT TO THE DATABASE
		require_once(P1_ROOT . '/system/controllers/database.php');
	}

	public function defineConst() {
		//DEFINE FILES
		$inifile = parse_ini_file(P1_ROOT . 'application.ini',true);
		$version = fopen(P1_ROOT . ".version", "r") or die('Please re-install the P1 framework or place the .version file into the root of the project.');
		//DEFINE CONSTANTS
		define("P1_URL",$inifile['application']['url']);
		define("P1_VERSION",fread($version,filesize(P1_ROOT . ".version")));
		define("SITE_NAME",$inifile['application']['name']);
		define("OWNER_NAME",$inifile['owner']['name']);
		define("OWNER_EMAIL",$inifile['owner']['email']);
		define("TIMEZONE",$inifile['misc']['timezone']);
	}
	
	public function setErrorLevel() {
		if(ENVIRONMENT == "development"){
			error_reporting(E_ALL & ~E_NOTICE);
		}else{
			error_reporting(E_ERROR | E_PARSE);
		}
	}
	
	public function startup() {
		try{
			$this->checkini();
			$this->iniChecks();
			$this->defineConst();
			$this->setErrorLevel();
		}catch(Exception $e){
			echo $e->getMessage();
			die;
		}
	}
	
	public function iniGet($ini1,$ini2) {
		$inifile = parse_ini_file(P1_ROOT . 'application.ini',true);
		
		return $inifile[$ini1][$ini2];
	}
}