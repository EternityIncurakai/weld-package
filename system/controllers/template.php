<?php
/*
 *
 * This template class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class template extends err {
	
	function __construct() {
	}
	
	public function getMeta($page,$url) {
		$urlc = rtrim($url, '/');
		//SPLIT URL
		$urlc = explode("/", $urlc);
		
		//VERIFY AND SET PAGE
		if(!isset($urlc[1])){
			if(file_exists("application/" . $urlc[0])){
				$pageUrl = "application/" . $urlc[0] . "/";
			}else{
				$pageUrl = "application/main/" . $urlc[0];
			}
		}else{
			$pageUrl = "application/" . $urlc[0] . "/";
		}
	
		$err = new err;
		$metaPath = P1_ROOT . $pageUrl . $page . "/info.json";
		$tplPath = P1_ROOT . "customisation/assets/meta.tpl";
		$metaFile = fopen($metaPath, "r") or $err->pageBuild('meta',$metaPath);
		$metaTPL = fopen($tplPath, "r") or $err->pageBuild('meta',$metatpl);
		$metaFile = fread($metaFile,filesize($metaPath));
		$metaTPL = fread($metaTPL,filesize($tplPath));
		$metaFile = json_decode($metaFile, true);
		
		//FIND ALL VALUES THAT NEED REPLACING
		preg_match_all('/{{(.*)}}/',$metaTPL, $tplVars);
		foreach ($tplVars[1] as $value) {
			$value1 = $value;
			$value1 = explode(':', $value1);
			$thing = "{{" . $value1[0] . ":" . $value1[1] . "}}";
			if($value1[0] == "sub"){
				$metaTPL = str_replace($thing, $metaFile[$value1[1]], $metaTPL);
			}elseif($value1[0] == "subs"){
				$metaTPL = str_replace($thing, P1_URL . '/' . $url, $metaTPL);
			}else{
				$err->pageBuild('invalidmeta',$tplPath);
			}
		}
		return $metaTPL;
	}
	
	public function getPage($page,$url) {
		$urlc = rtrim($url, '/');
		//SPLIT URL
		$urlc = explode("/", $urlc);
		
		//VERIFY AND SET PAGE
		if(!isset($urlc[1])){
			if(file_exists("application/" . $urlc[0])){
				$page = "application/" . $urlc[0] . "/";
			}else{
				$page = "application/main/" . $urlc[0];
			}
		}else{
			$pageUrl = "application/" . $urlc[0] . "/";
		}
		
		//DEFINE CLASSES
		$err = new err;
		try{
			$theme = new theme;
			$theme->construct($url);
		}catch(Exception $e){
			echo $e->getMessage();
			die;
		}
		//GET INFO FILE
		$infoPath = P1_ROOT . $pageUrl . $page . "/info.json";
		$infoFile = fopen($infoPath, "r") or $err->pageBuild('pagetpl',$infoPath);
		$infoFile = fread($infoFile, filesize($infoPath));
		$infoFile = json_decode($infoFile, true);
		//GET TPL
		$tplPath = $GLOBALS["THEME_DIR"] . "pages/" . $infoFile['type'] . ".tpl";
		$pageTpl = fopen($tplPath, "r") or $err->pageBuild('pagetpl',$tplPath);
		$pageTpl = fread($pageTpl,filesize($tplPath));
		//GET THEME INFO
		$themePath = $GLOBALS["THEME_DIR"] . "/manifest.json";
		$themeInfo = fopen($themePath, "r") or $err->pageBuild('themeInfo',$themePath);
		$themeInfo = fread($themeInfo,filesize($themePath));
		
		//FIND ALL VALUES THAT NEED REPLACING
		preg_match_all('/{{(.*)}}/', $pageTpl, $tplVars);
		foreach ($tplVars[1] as $value) {
			$value1 = $value;
			$value1 = explode(':', $value1);
			if($value1[0] == "sub"){
				$pageTpl = str_replace("{{" . $value1[0] . ":" . $value1[1] . "}}", $infoFile[$value1[1]], $pageTpl);
			}elseif($value1[0] == "theme"){
				$pageTpl = str_replace("{{" . $value1[0] . ":" . $value1[1] . "}}", $theme->getLink($value1[1]), $pageTpl);
			}elseif($value1[0] == "themeasset"){
				if(file_exists($GLOBALS["THEME_ASSETS"] . $value1[1])){
				ob_start();
				include($GLOBALS["THEME_ASSETS"] . $value1[1]);
				$themeasset = ob_get_clean();
				$pageTpl = str_replace("{{" . $value1[0] . ":" . $value1[1] . "}}", $themeasset, $pageTpl);
				}else{
					$err->pageBuild('themeAsset',$value1[1]);
					$pageTpl = str_replace("{{" . $value1[0] . ":" . $value1[1] . "}}", "", $pageTpl);
				}
			}elseif($value1[0] == "content"){
				ob_start();
				include P1_ROOT . $pageUrl . $page . "/content.php";
				$content = ob_get_clean();
				$pageTpl = str_replace("{{" . $value1[0] . "}}", $content, $pageTpl);
			}elseif($value1[0] == "metaInfo"){
				$pageTpl = str_replace("{{" . $value1[0] . "}}", $this->getMeta($page,$url), $pageTpl);
			}else{
				$err->pageBuild('pagetpl',$tplPath);
			}
		}
		return $pageTpl;
	}
	
}
