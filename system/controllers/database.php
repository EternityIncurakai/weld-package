<?php
/*
 *
 * This database class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class database extends PDO {
	
	public $db;
	
	function __construct() {
	}
	
	public function setupDb() {
		//INCLUDE STARTUP
		require_once('startup.php');
		$start = new Startup;
		
		$con = $start->iniGet('database','dbcon');
		$name = $start->iniGet('database','dbname');
		$user = $start->iniGet('database','dbuser');
		$pass = $start->iniGet('database','dbpass');
		try{
			//CONSTRUCT CONNECTION
			$this->db = new \PDO("mysql:host=" . $con . ";dbname=" . $name, $user, $pass);
			$GLOBALS['db'] = $this->db;
		}catch(PDOException $e){
			require_once(P1_ROOT . '/system/controllers/error.php');
			$err = new err;
			try{
				if(ENVIRONMENT == "development"){
					$err->fatal("Error connecting to database","Error details (hidden in production): " . $e->getMessage());
				}else{
					$err->fatal("Error connecting to database","More error details available in development mode.");
				}
			}catch(Exception $e){
				echo $e->getMessage();
				die;
			}
		}
	}
}
?>