<?php
/*
 *
 * This bootstrap class is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

class bootstrap extends startup {
	
	private $pagesUrl;
	
	function __construct() {
	}
	
	public function checkMaintenance() {
		require_once('system/controllers/error.php');
		$err = new err;
		
		$inifile = parse_ini_file('application.ini',true);
		if($inifile['maintenance']['active'] == "true"){
			if($inifile['maintenance']['type'] == "light"){
				$err->lightMaintenance();
			}elseif($inifile['maintenance']['type'] == "heavy"){
				$end = $inifile['maintenance']['end'];
				$reason = $inifile['maintenance']['reason'];
				$err->heavyMaintenance($end, $reason);
			}elseif($inifile['maintenance']['type'] == "scheduled"){
				$end = $inifile['maintenance']['end'];
				$reason = $inifile['maintenance']['reason'];
				$err->schMaintenance($end, $reason);
			}
		}
	}
	
	public function buildPage($url) {
		//SET ERRORS
		require_once P1_ROOT . "/system/controllers/error.php";
		$err = new err;
		
		//START THEMES
		require_once('system/controllers/themes.php');
		try{
			$theme = new theme;
			$theme->construct($url);
			$theme->setConstants();
			$theme->verify();
		}catch(Exception $e){
			echo $e->getMessage();
			die;
		}
		
		$urlc = rtrim($url, '/');
		//SPLIT URL
		$urlc = explode("/", $urlc);
		
		//VERIFY AND SET PAGE
		if(!isset($urlc[1])){
			if($urlc[0] == ""){
				$urlc[0] = "home";
			}
			if(file_exists("application/" . $urlc[0])  && $urlc[0] != ""){
				$page = "application/" . $urlc[0] . "/home";
				$url2 = $urlc[0] . "/home";
				$page2 = "home";
			}else{
				$page = "application/main/" . $urlc[0];
				$url2 = "main/" . $urlc[0];
				$page2 = $urlc[0];
			}
		}elseif(!isset($urlc[2])){
			$page = "application/" . $urlc[0] . "/" . $urlc[1];
			$url2 = $urlc[0] . "/" . $urlc[1];
			$page2 = $urlc[1];
		}else{
			$page = $url;
			$err->pageBuild('path',$page);
			return;
		}
		
		//CHECK IF THE PAGE EXISTS
		if(!file_exists($page)){
			$err->pageBuild('404',$page);
			return;
		}
		
		//SET PAGE VARIABLES
		require_once(P1_ROOT . 'system/controllers/template.php');
		$tpl = new template;
		
		echo $tpl->getPage($page2,$url2);
	}
	
	public function getAppDir($url) {
		//SET ERRORS
		require_once P1_ROOT . "/system/controllers/error.php";
		$err = new err;
		
		$urlc = rtrim($url, '/');
		//SPLIT URL
		$urlc = explode("/", $urlc);
		
		//VERIFY AND SET PAGE
		if(!isset($urlc[1])){
			if(file_exists("application/" . $urlc[0] . "/application.ini") && $urlc[0] != ""){
				$page = "application/" . $urlc[0] . "/";
			}else{
				$page = "application/main/";
			}
		}elseif(!isset($urlc[2])){
			$page = "application/" . $urlc[0] . "/";
		}else{
			return;
		}
		
		return $page;
	}
	
	
	
}
