# Weld Package #

The weld package consists of the Weld CMS and the P1 Framework. This package is the base for most EterniTech sites that are built quickly and effectively, this package is also free for public use under the licences documented in this readme file.

### Installation ###

(currently unfinished, you may have to install manually.)
To install the package please place the contents of this repository into an apache server environment with PHP running and open the installation directory in a browser, follow the instructions and Weld will install itself.

### Licences ###

Weld is released under the licence included in this repository under licence.txt please review this licence for how Weld is licensed to you.

### Development ###
Weld is currently unfinished and is under continuous development, please bear with us as bugs are fixed and new featured completed.
The weld panel is severely unfinished but is under constant development at this moment in time.