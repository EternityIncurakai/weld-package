<!-- META INFO -->
		<meta charset="utf-8">
		<meta name="description" content="{{sub:description}}" />
		<meta property="og:title" content="{{sub:title}}" />
		<meta property="og:url" content="{{subs:url}}" />
		<meta name="twitter:description" content="{{sub:description}}">
		<meta name="twitter:url" content="{{subs:url}}">
		<link rel="publisher" href="{{sub:publisher}}" />
		<title>{{sub:title}}</title>