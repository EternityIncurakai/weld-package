{{themeasset:checkPrivilleges.php}}
<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body>
		{{content}}
	</body>
</html>