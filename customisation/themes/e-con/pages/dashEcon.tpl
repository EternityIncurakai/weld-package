{{themeasset:checkPrivilleges.php}}
<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body>
		<div class="econ_bar topBar">
			E-con Dashboard
		</div>
		<div class="inline sidebar">
			{{themeasset:sidebar.php}}
		</div><div class="inline main">
			<div class="card">
				{{content}}
			</div>
		</div>
	</body>
</html>