<!-- HEY!!! STOP LOOKING AT MY CODE -->
<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body>
		{{themeasset:header.php}}
		<div class="main"><br/><br/><br/>
			<div class="box">
				{{content}}<br/>
			</div><br/><br/>
		</div>
	</body>
</html>