<!-- HEY!!! STOP LOOKING AT MY CODE -->
<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body>
		{{themeasset:header.php}}<br/><br/>
		<div class="main">
			<div class="box" style="text-align:left;">
				<h2>Welcome</h2>
				Hello and welcome to your dashboard, here you can view the comments that people have posted to you anonymously, you can also view statistics on your posts in the sidebar on the right, anyway, enjoy! :)
			</div><br/><br/><hr/><br/><br/>
			{{content}}
		</div><br/><br/>
	</body>
</html>