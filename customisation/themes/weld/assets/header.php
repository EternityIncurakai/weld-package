<?php
	//INTIALISE THE USERS CLASS
	require_once(P1_ROOT . 'system/controllers/users.php');
	$user = new users;
	
	//CHECK IF THE USER IS LOGGED IN
	if(!isset($_SESSION['id'])){
		header('location:/weld/');
	}
?>
<div class="dash-header" style="text-align:center;">
	<div style="font-family:'Raleway','Arial';font-size:50px;font-weight:600;margin:0 auto;vertical-align:middle;display: block;vertical-align: middle;">
		Weld Panel
	</div><div class="logout" style="right:20px;top:20px;position:fixed;">
		Hi, <?php echo $user->name; ?>  //  <a href="/weld/logout" class="logout-a logout">Logout</a>
	</div>
</div>