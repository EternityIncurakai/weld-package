<!DOCTYPE html>
<html class="dash-body">
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body class="dash-body">
		{{themeasset:header.php}}
		<div class="dash_sidebar inline">
			{{themeasset:sidebar.php}}
		</div><div class="dash_main inline">
			<div class="dash-container">
				{{content}}
			</div>
		</div>
	</body>
</html>