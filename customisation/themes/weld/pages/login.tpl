<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body class="login-body">
		<div class="login-wrapper">
		{{themeasset:ver.php}}
			<div class="login_main">
				<img src="/customisation/themes/weld/assets/logo_white.svg" alt="weld logo" class="login-logo" /><br/><hr class="line-sep" /><br/>
				<div class="login-box">
					{{content}}
				</div>
			</div>
		</div>
	</body>
</html>