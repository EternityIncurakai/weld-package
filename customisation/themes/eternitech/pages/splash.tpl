<!DOCTYPE html>
<html>
    <head>
        <link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
        <style>
        	@charset "UTF-8";
			@import url(https://fonts.googleapis.com/css?family=Raleway|Roboto|Montserrat:400,700|Open+Sans:600);

            body, html{
                padding:0;
                margin:0;
                height:100%;
                width:100%;
                text-align:center;
                background-color:#303030;
            }
            .green{
                background-color:#92d050;
                top:0;
                position:fixed;
            }
            .blue{
                background-color:#00b0f0;
                bottom:0;
                position:fixed;
            }
            .smallBar{
                height:20px;
                width:100%;
                
            }
            .content{
            	color:#fff;
            	font-family:'Montserrat','Calibri';
            }
        </style>
    </head>
    <body>
        <div class="smallBar green"><div><br/><br/><br/><br/><br/><br/>
        <img src="http://eternitech.tk/customisation/assets/logo.png" alt="logo" style="width:60%;text-align:center;" /><br/><br/><br/><br/><br/>
        <div class="content">{{content}}</div>
        <div class="smallBar blue"><div>
    </body>
</html>