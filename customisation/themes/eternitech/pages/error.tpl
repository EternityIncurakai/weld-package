<!DOCTYPE html>
<html>
	<head>
		<link href="{{theme:CSSLink}}" rel="stylesheet" type="text/css" />
		<link rel="icon" href="{{theme:faviconLink}}" type="image/icon" />
		{{metaInfo}}
	</head>
	<body>
		{{themeasset:header.php}}
		<div class="topBox error-top">
			<h1 class="tob-box-content">{{sub:subTitle}}</h1>
		</div>
		<div class="main error-cont">
			<br/><br/>{{content}}<br/>
		</div>
		{{themeasset:footer.php}}
	</body>
</html>