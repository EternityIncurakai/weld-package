<?php
/*
 *
 * This page's content is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

//REQUIRE USER CLASS
require_once P1_ROOT . '/system/controllers/users.php';
$usr = new users;

//CHECK IF ALREADY LOGGED IN
if(isset($_SESSION['id'])){
	header('location:/weld/dash/');
}

//IF FORM FILLED, PROCESS
if(isset($_POST['email'])){
	if($_POST['email'] == "" || $_POST['password'] == "" || $_SESSION['id'] != ""){
		$error = "Please fill out the form correctly or log-out to continue.";
	}else{
		if($usr->authUser($_POST['email'],$_POST['password']) == "1"){
			//IF ADMIN, RE-DIRECT TO PANEL
			if(($usr->permissions >= 3) || (!isset($_GET['redir']))){
				header('location:/weld/dash');
			}else{
				header('location:/' . $_GET['redir']);
			}
		}else{
			$error = $usr->authUser($_POST['email'],$_POST['password']);
		}
	}
}?>
<?php if(isset($error)){echo "<div class=\"error\">" . $error . "</div><br/>";} ?>
<form action="" method="POST">
	<label style="text-align:left;">Email Address</label><br/>
	<input type="email" placeholder="steve@example.com" class="login-input" name="email"/><br/>
	<label>Password</label><br/>
	<input type="password" placeholder="chickenToastie2k15" class="login-input" name="password"/><br/><br/>
	<input type="submit" class="button login-input" value="Login" />
</form>