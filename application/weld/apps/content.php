<?php
/*
 *
 * This page's content is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */
 ?>
 <h1>Installed Applications</h1>
 <?php
 error_reporting(E_ALL);
 $appsFolder = scandir(P1_ROOT . 'application');
 $appsFolderno = count($appsFolder) - 2;
 $i = 0;
 while($i < $appsFolderno){ ?>
	<br/><br/>
	<div class="card">
		<h2><?= $appsFolder[$i + 2]; ?></h2>
	</div>
	<?php 
	$i++; 
 } ?>
 <br/><br/>
