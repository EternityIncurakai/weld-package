<?php
/*
 *
 * This page's content is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */
 
 //CALL DEPENDENCY CLASSES
 require_once(P1_ROOT . 'system/controllers/weldInfo.php');
 $info = new info;
?>
<br/><br/>
<div class="inline col">
	<div class="card">
		<h2>Overview</h2>
		<h4>Some info from your site.</h4>
		<div style="text-align:center;"><br/><br/>
			<div class="inline" style="width:32%;">
				<span style="font-size:40px;"><?php echo $info->getAppNo(); ?></span><br/>
					Installed Apps
					
			</div><div class="inline" style="width:32%;">
				<span style="font-size:40px;"><?php echo $info->getAccNo(); ?></span><br/>
					Registered Account<?php if($info->getAccNo() > 1){echo "s";} ?>
				
			</div><div class="inline" style="width:32%;">
				<span style="font-size:40px;"><?php echo $info->getThemeNo(); ?></span><br/>
					Installed Themes
				
			</div>
		</div><br/>
	</div>
</div><div class="inline col">
	<div class="card">
		<h2>Update</h2>
		<h4>Am I up to date?</h4><br/>
		<div style="text-align:center">
			<?php
			if($info->upToDate() == 1){
				?><img src="/customisation/themes/weld/assets/tick.svg" style="-webkit-filter: drop-shadow(5px 5px 6px rgba(0,0,0,0.3)); filter: url(/customisation/themes/weld/assets/tick.svg#drop-shadow);width:40%;" alt="woo!" />
				<h3>Weld is up to date!</h3><?php
			}else{
				?><img src="/customisation/themes/weld/assets/cross.svg" style="-webkit-filter: drop-shadow(5px 5px 6px rgba(0,0,0,0.3)); filter: url(/customisation/themes/weld/assets/cross.svg#drop-shadow);width:40%;" alt="aww!" />
				<h3>Weld is out of date.</h3><br/>Go to the settings section to update.<?php
			} ?>
		</div><br/>
	</div><br/><br/>
	<div class="card">
		<h2>Analytics</h2>
		<h4>How many people view your site?</h4>
		<table class="WeldTable">
			<tr><th style="width:70%;">Day</th><th style="width:15%;">Unique Viewers</th><th style="width:15%;">View Amount</th></tr>
			<tr style="background-color:#ebf3ed;"><td>Monday</td><td>150</td><td>600</td></tr>
			<tr><td>Tuesday</td></tr>
			<tr style="background-color:#ebf3ed;"><td>Wednesday</td></tr>
			<tr><td>Thursday</td></tr>
			<tr style="background-color:#ebf3ed;"><td>Friday</td></tr>
			<tr><td>Saturday</td></tr>
			<tr style="background-color:#ebf3ed;"><td>Sunday</td></tr>
		</table>
		<?php if($info->googAnalytics('on') == FALSE){
			echo "<br/><div style=\"text-align:center;\">To enable google analytics, please go to the settings section.</div><br/>";
		} ?>
	</div><br/><br/>
</div>
