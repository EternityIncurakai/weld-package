<?php
/*
 *
 * This index dispatcher is part of the Weld Package and P1 Framework
 * 
 * The P1 Framework and Weld CMS package is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @license GPL
 * @license http://gnu.org/licenses/gpl-3.0.txt
 *
 * @author Peter Richards <peter.richards@p110tech.co.uk>
 * @version v0.2
 * @package Weld
 *
 */

try{
	//BEGIN STARTUP
	require_once('system/controllers/startup.php');
	$start = new startup;
	$start->startup();
}catch(Exception $e){
	echo $e->getMessage();
	die;
}

//DEFINE ERRORS
require_once('system/controllers/error.php');
$err = new err;

//BEGIN BOOTSTRAP
require_once('system/controllers/bootstrap.php');
$bs = new bootstrap;
$bs->checkMaintenance();

//SETUP DATABASE
require_once('system/controllers/database.php');
$db = new database;
$db->setupDb();

//BUILD PAGE
try{
	$bs->buildPage($_GET['url']);
}catch(Exception $e){
	echo $e->getMessage();
	die;
}